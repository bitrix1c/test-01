<?php

namespace App\Components;

use App\ActionFilters\IpAddrValidateActionFilter;
use App\Exceptions\ComponentException;
use App\Service\IpChecker\Sypexgeo;
use App\Service\IpChecker\SypexgeoIpChecker;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\Json;
use CBitrixComponent;
use CJSCore;
use Sale\Handlers\Delivery\Additional\CacheManager;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

class IpChecker extends CBitrixComponent implements Controllerable
{

    /**
     * @var string[] Модули от которых зависит работа компонента
     */
    private $requiredModules = [
        'highloadblock',
    ];

    /**
     * @var string имя класса HL блока с данными
     */
    private $entityDataClass;

    /**
     * @var SypexgeoIpChecker сервис отвечающий за проверку ip адреса и получения данных о нём от удалённого сервера
     */
    private SypexgeoIpChecker $service;

    /**
     * @inheritdoc
     * @param $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params['CACHE_TIME'] = $params['CACHE_TIME'] ?: 3600;
        $params['CACHE_TYPE'] = $params['CACHE_TYPE'] ?: 'A';
        $params['DEFAULT_IP'] = $params['DEFAULT_IP'] ?: '109.248.252.95';
        $params['HIGHLOAD_BLOCK'] = $params['HIGHLOAD_BLOCK'] ?: 'IpAddressData';
        return $params;
    }

    /**
     * @inheritdoc
     * @return void
     * @throws ComponentException
     * @throws LoaderException
     * @throws SystemException
     */
    public function executeComponent(): void
    {
        $this->init();
        if ($this->startResultCache()) {
            try {
                $this->arResult['SERVICE_NAME'] = $this->service->getApiServiceName();
            } catch (\Exception $exception) {
                $this->abortResultCache();
            }

            $this->includeComponentTemplate();
        }
    }

    /**
     * Ajax действие для обработки полученного в гет-параметрах ip адреса
     * -
     * TODO: В данной реализации ip адрес валидируется на уровне orm, необходимо сделать валидатор на уровне роута или
     * префильтра
     * TODO: Отправку email необходимо вынести в логгер
     * @param string $ipAddress - ip адрес в виде строки
     * @return array
     * @throws ComponentException
     * @throws LoaderException
     * @throws ArgumentException
     * @throws SystemException
     */
    public function checkIpAction(string $ipAddress): array
    {
        if (!$ipAddress) {
            throw new ComponentException($this->getName(), 'IP адрес не указан');
        }

        $this->init();
        $data = [];

        $cacheId = md5(serialize($this->arParams) . '_' . $this->getName() . '_' . md5($ipAddress));
        $cacheDir = '/ipchecker/'.md5($ipAddress);
        $cache = Cache::createInstance();

        if ($cache->initCache($this->arParams['CACHE_TIME'], $cacheId, $cacheDir)) {
            $cacheVars = $cache->GetVars();
            $data = $cacheVars['data'];
        } elseif ($cache->startDataCache()) {
            $service = $this->service;
            $query = $this->entityDataClass::query();

            $ipAddressData = $query->setSelect(['*'])
                ->where('UF_IP_ADDRESS', $ipAddress)
                ->fetchObject();

            if (!$ipAddressData) {
                $service->setIpAddress($ipAddress);
                $data = $service->check();

                $fields = [
                    'UF_IP_ADDRESS' => $ipAddress,
                    'UF_CITY' => $data['city']['name_ru'],
                    'UF_COUNTRY' => $data['country']['name_ru'],
                    'UF_LOCATION' => $data['city']['lat'] . ', ' . $data['city']['lon'],
                    'UF_JSON_DATA' => Json::encode($data),
                ];

                /** @var AddResult $result */
                $result = $this->entityDataClass::add($fields);
                if (!$result->isSuccess()) {
                    $cache->abortDataCache();
                    $errorMessage = 'Ошибка добавления записи в HL';
                    if (($errors = $result->getErrorMessages()) && ($errorDescription = reset($errors))) {
                        $errorMessage .= ': ' . $errorDescription;
                    }
                    throw new ComponentException($this->getName(), $errorMessage);
                }

                $ipAddressData = $result->getObject();
            }

            $data = [
                'country' => $ipAddressData->get('UF_COUNTRY'),
                'city' => $ipAddressData->get('UF_CITY'),
                'ip_address' => $ipAddressData->get('UF_IP_ADDRESS'),
                'service_name' => $service->getApiServiceName(),
            ];

            $cache->endDataCache(['data' => $data]);
        }

        return $data;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function configureActions(): array
    {
        return [
            'checkIp' => [
                '-prefilters' => [
                    Authentication::class,
                ],
                'prefilters' => [
                    new IpAddrValidateActionFilter(),
                ]
            ],
        ];
    }

    /**
     * Инициализация класса сущности HL блока в свойство класса компонента $entityDataClass
     * @return void
     * @throws SystemException
     */
    private function initHlBlockEntity(): void
    {
        /** @var HL\EO_HighloadBlock_Entity $entity */
        $entity = HL\HighloadBlockTable::compileEntity($this->arParams['HIGHLOAD_BLOCK']);
        $this->entityDataClass = $entity->getDataClass();
    }

    /**
     * Общий метод инициализации компонента
     * @return void
     * @throws ComponentException
     * @throws LoaderException
     * @throws SystemException
     */
    private function init(): void
    {
        foreach ($this->requiredModules as $moduleId) {
            if (!Loader::includeModule($moduleId)) {
                throw new ComponentException($this->getName(), "Модуль {$moduleId} не установлен");
            }
        }

        CJSCore::Init(['jquery', 'fx']);
        $this->initHlBlockEntity();
        $this->service = new SypexgeoIpChecker();
    }


}