<?php use Bitrix\Main\Context;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var array $templateData */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row mb-3">

    <form class="col-md-8 col-sm-12" id="ipCheckerForm">
        <label for="inputIpAddr" class="col-sm-2 col-form-label">Ваш IP адрес:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputIpAddr">
        </div>
        <button type="submit" class="btn btn-primary">Проверить</button>
        <button type="button" class="btn btn-secondary" data-clear="true">Очистить</button>
    </form>
    <div class="col-md-4 col-sm-12" >
        <div class="info">
            <div class="field" id="serviceName">Данные предоставлены сервисом: <span class="value"><?= $arResult['SERVICE_NAME'] ?: '-' ?></span></div>
            <div class="field" id="ipAddress">IP адрес: <span class="value">-</span></div>
            <div class="field" id="country">Страна: <span class="value">-</span></div>
            <div class="field" id="city">Город: <span class="value">-</span></div>
            <div class="alert alert-danger" id="error" role="alert"></div>
        </div>
    </div>

</div>

