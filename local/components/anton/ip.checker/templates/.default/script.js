$(document).ready(function () {
    $('#error').hide();
    $(document).on('submit', '#ipCheckerForm', function (e) {
        e.preventDefault();

        BX.ajax.runComponentAction('anton:ip.checker', 'checkIp', {
            mode: 'class',
            data: {
                ipAddress: $('#inputIpAddr').val()
            },
        }).then(function (response) {
            console.log(response);
            let data = response.data;
            // BX.IpChecker.fillResult(data);
            $('#error').hide();
            BX.IpChecker.fillInfo(data);

        }, function (response) {
            console.log(response);
            console.log(response.errors[0].message);
            $('#error').html(response.errors[0].message);
            $('#error').show();
            BX.IpChecker.clearInfo();

        });
    });

    $(document).on('click', '.btn[data-clear="true"]', function (e) {
        e.preventDefault();
        BX.IpChecker.clearInfo();
        $('#ipCheckerForm')[0].reset();
        $('#error').hide();
    });
});

BX.IpChecker = {
    fillInfo: function (data) {
        // alert(1)
        $('#ipAddress > .value').text(data.ip_address);
        $('#city > .value').text(data.city ? data.city : '-');
        $('#country > .value').text(data.country ? data.country : '-');
    },
    clearInfo: function () {
        $('#ipAddress > .value').text('-');
        $('#city > .value').text('-');
        $('#country > .value').text('-');
    }
};