<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @global \CMain $APPLICATION ; */

?>
    </main>
	<footer></footer>
    <?php $APPLICATION->ShowBodyScripts();?>
  </body>
</html>
