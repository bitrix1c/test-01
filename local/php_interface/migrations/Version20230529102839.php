<?php

namespace Sprint\Migration;


class Version20230529102839 extends Version
{
    protected $description = "почтовое событие для отправки уведомлений об ошибках";

    protected $moduleVersion = "4.3.1";

    /**
     * @return bool|void
     * @throws Exceptions\HelperException
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $helper->Event()->saveEventType('EXCEPTION_ALERT', [
            'LID' => 'ru',
            'EVENT_TYPE' => 'email',
            'NAME' => 'Сообщения об ошибках',
            'DESCRIPTION' => '
                #MESSAGE# - Сообщение об ошибке
                #CODE# - Код ошибки
                #TIME# - Дата и время возникновения ошибки
            ',
            'SORT' => '150',
        ]);
        $helper->Event()->saveEventMessage('EXCEPTION_ALERT', [
            'LID' =>
                [
                    0 => 's1',
                ],
            'ACTIVE' => 'Y',
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO' => '#DEFAULT_EMAIL_FROM#',
            'SUBJECT' => 'Произошла ошибка #CODE# при проверке IP адреса',
            'MESSAGE' => 'Дата и время: #TIME#<br>Код: #CODE#<br>Сообщение: #MESSAGE#<br>',
            'BODY_TYPE' => 'html',
            'BCC' => '',
            'REPLY_TO' => '',
            'CC' => '',
            'IN_REPLY_TO' => '',
            'PRIORITY' => '',
            'FIELD1_NAME' => '',
            'FIELD1_VALUE' => '',
            'FIELD2_NAME' => '',
            'FIELD2_VALUE' => '',
            'SITE_TEMPLATE_ID' => '',
            'ADDITIONAL_FIELD' =>
                [
                ],
            'LANGUAGE_ID' => '',
            'EVENT_TYPE' => '[ EXCEPTION_ALERT ] Сообщения об ошибках',
        ]);
    }

    /**
     * @return bool|void
     * @throws Exceptions\HelperException
     */
    public function down()
    {
        $helper = $this->getHelperManager();
        $helper->Event()->deleteEventMessage([
            'EVENT_NAME' => 'EXCEPTION_ALERT',
            'SUBJECT' => 'Произошла ошибка #CODE# при проверке IP адреса',
        ]);
        $helper->Event()->deleteEventType(
            [
                'LID' => 'ru',
                'EVENT_TYPE' => 'email',
                'EVENT_NAME' => 'EXCEPTION_ALERT',
            ]
        );
    }
}
