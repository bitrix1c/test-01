<?php

declare(strict_types = 1);

namespace App\ActionFilters;

use App\Helpers\AppHelper;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Error;
use Bitrix\Main\Event;
use Bitrix\Main\EventResult;

/**
 * Класс IpAddrValidateActionFilter
 * Содержит логику валидации ip адреса
 */
class IpAddrValidateActionFilter extends ActionFilter\Base
{
    /**
     * метод-обработчик события сработает перед вызово экшена
     * @param Event $event
     * @return EventResult|null
     */
    public function onBeforeAction(Event $event): ?EventResult
    {
        $request = $this->getAction()->getController()->getRequest();
        $ipAddress = $request->get('ipAddress');

        if (!$ipAddress) {
            $this->addError(new Error('IP адрес не указан'));
        }

        if (!preg_match(AppHelper::REGEX_IP_ADDR, $ipAddress)) {
            $this->addError(new Error('Некорректный IP адрес'));
        }

        if ($this->errorCollection->count() > 0) {
            return new EventResult(EventResult::ERROR, null, null, $this);
        }

        return null;
    }

}