<?php

declare(strict_types = 1);

namespace App\Exceptions;

use Bitrix\Main\Type\DateTime;

/**
 * Класс BaseException
 * Основной класс исключений, в котором могут быть подключены логгеры и серивсы реализующие необходимую бизнес-логику
 */
class BaseException extends \Exception
{
    /**
     * @param string $message - сообщение об ошибке
     * @param int $code - код ошибки
     * @param Throwable|null $previous
     * @param bool $sendEmailAlert - флаг для отправки email
     */
    public function __construct(
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null,
        bool $sendEmailAlert = false
    ) {
        //TODO: Вынести отправку исключений, так же как и сохранение их в файл в отдельный логгер
        if ($sendEmailAlert === true) {
            \Bitrix\Main\Mail\Event::send([
                'EVENT_NAME' => 'EXCEPTION_ALERT',
                'LID' => SITE_ID,
                'C_FIELDS' => [
                    'MESSAGE' => $message,
                    'CODE' => $code,
                    'TIME' => (new DateTime())->format('d.m.Y H:i:s'),
                ],
            ]);
        }

        parent::__construct($message, $code, $previous);
    }
}