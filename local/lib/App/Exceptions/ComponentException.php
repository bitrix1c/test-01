<?php

declare(strict_types = 1);

namespace App\Exceptions;

/**
 * Класс ComponentException
 * Для обработки исключений в кастомных компонентах
 */
class ComponentException extends BaseException
{
    /**
     * @param string $componentName имя компонента, в котором выбрасывается исключение
     * @param string $message сообщение об ошибке
     * @param int $code код ошибки
     * @param Throwable|null $previous
     */
    public function __construct(
        string $componentName = '',
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null
    ) {

        $message = "Ошибка компонента \"{$componentName}\": <br><b>{$message} {$this->getMessage()}</b>";
        parent::__construct($message, $code, $previous, true);

    }

}