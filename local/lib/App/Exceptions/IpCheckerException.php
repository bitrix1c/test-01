<?php

declare(strict_types = 1);

namespace App\Exceptions;

/**
 * Класс IpCheckerException
 * Обработка ошибок сервиса IpChecker
 */
class IpCheckerException extends BaseException
{
    /**
     * @param string $message сообщение об ошибке
     * @param int $code код ошибки
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous, true);
    }

}