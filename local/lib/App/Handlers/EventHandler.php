<?php

declare(strict_types = 1);

namespace App\Handlers;

use Bitrix\Main\Application;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;


class EventHandler
{
    /** @var EventHandler инстанс класса-синглтона */
    private static $instance;

    /**
     * возвращает инстанс класс
     * @return EventHandler
     */
    public static function getInstance(): EventHandler
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Метод инициализации в котором происходит связка событий и обработчиков событий
     * @return void
     */
    public function init(): void
    {
        $eventManager = EventManager::getInstance();
        //событие IpAddressDataOnAfterUpdate после обновления записи в HL блоке IpAddressData
        $eventManager->addEventHandler(
            '',
            'IpAddressDataOnAfterUpdate',
            [self::class, 'clearIpAddressDataCache']
        );
    }

    /**
     * Очистка кеша конкретного элемента hl блока IpAddressData
     * @param Event $event
     * @return void
     */
    public static function clearIpAddressDataCache(Event $event): void
    {
        $params = $event->getParameters();
        $ipAddress = $params['fields']['UF_IP_ADDRESS']['VALUE'];
        if (!$ipAddress) {
            return;
        }
        $cache = Application::getInstance()->getCache();
        $cache->cleanDir('/ipchecker/' . md5($ipAddress));
    }


    /**
     * закрытый метод, может содержать бизнес логику
     */
    private function __construct()
    {
    }

    /**
     * закрытый метод, может содержать бизнес логику
     * @return void
     */
    private function __wakeup()
    {
    }

    /**
     * закрытый метод, может содержать бизнес логику
     * @return void
     */
    private function __clone()
    {
    }

}