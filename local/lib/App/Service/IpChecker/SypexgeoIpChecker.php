<?php

declare(strict_types = 1);

namespace App\Service\IpChecker;

use App\Exceptions\IpCheckerException;
use Bitrix\Main\ArgumentException;

class SypexgeoIpChecker extends Base\IpChecker
{

    protected string $apiServiceName = 'sypexgeo.net';
    protected string $apiBaseUrl = 'https://api.sypexgeo.net/json/';

    /**
     * @param string|null $ipAddress
     */
    public function __construct(?string $ipAddress = null)
    {
        if ($ipAddress) {
            $this->ipAddress = $ipAddress;
        }
    }

    public function check(): array
    {
        if (!$this->ipAddress) {
            throw new IpCheckerException("Не задан ip адрес");
        }
        $this->url = $this->apiBaseUrl . $this->ipAddress;
        return parent::check();
    }
}