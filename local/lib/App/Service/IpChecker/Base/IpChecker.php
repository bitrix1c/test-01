<?php

declare(strict_types = 1);

namespace App\Service\IpChecker\Base;

use App\Exceptions\IpCheckerException;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Json;

abstract class IpChecker
{
    protected string $apiServiceName;
    protected string $apiBaseUrl;
    protected string $ipAddress;
    protected string $url;

    private HttpClient $httpClient;

    /**
     * @throws SystemException
     * @throws ArgumentException
     */
    public function check(): array
    {
        if (!$this->url) {
            throw new IpCheckerException('Не задан url к сервису проверк ip-адреса');
        }
        $this->httpClient = new HttpClient();
        if (!$answer = $this->httpClient->get($this->url)) {
            throw new IpCheckerException(
                "Ответ от сервиса {$this->apiServiceName} не получен или получен с ошибкой. <br> <b>Проверьте подключение к сети!</b>"
            );
        }
        return Json::decode($answer);
    }

    /**
     * @return string
     */
    public function getApiServiceName(): string
    {
        return $this->apiServiceName;
    }

    /**
     * @return string
     */
    public function getApiBaseUrl(): string
    {
        return $this->apiBaseUrl;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress): void
    {
        $this->ipAddress = $ipAddress;
    }

}