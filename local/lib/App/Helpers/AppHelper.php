<?php

declare(strict_types = 1);

namespace App\Helpers;

class AppHelper
{
    public const REGEX_IP_ADDR = "/^([0-9]{1,3}[\.]){3}[0-9]{1,3}$/";
}