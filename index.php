<?php

/** @global \CMain $APPLICATION */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Тест: Проверка ip адреса");

$APPLICATION->IncludeComponent('anton:ip.checker', '', [
    'CACHE_TYPE' => 'A',
    'CACHE_TIME' => 86000,
]);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>